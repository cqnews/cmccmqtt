package com.link510.nbiot.cmcc.domain.subscription;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.link510.nbiot.cmcc.config.Config;
import com.link510.nbiot.cmcc.domain.BaseDTO;
import lombok.*;

import java.util.Map;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SendSubscriptionDTO implements BaseDTO {

    private static final long serialVersionUID = 5002632324631357076L;

    /**
     * 设备imei号，平台唯一，必填参数
     */
    @Builder.Default
    private String imei = "";

    /**
     * 设备的object id，根据终端设备sdk确定
     */
    @JSONField(name = "obj_id")
    @Builder.Default
    private Integer objId = 3200;

    /**
     * nbiot设备object下具体的一个实例id,根据终端设备sdk确定
     */
    @JSONField(name = "obj_inst_id")
    @Builder.Default
    private Integer objInstId = 0;


    /**
     * nbiot设备的资源id,根据终端设备sdk确定
     */
    @JSONField(name = "res_id")
    @Builder.Default
    private Integer resId = 0;


    /**
     * true为取消订阅，false为订阅
     */
    @Builder.Default
    private boolean cancel = false;


    /**
     * 上传数据的最小时间间隔，默认为0，此时有数据就上传
     */
    @Builder.Default
    private Integer pmin = 0;

    /**
     * 上传数据的最大时间间隔
     */
    @Builder.Default
    private Integer pmax = 0;

    /**
     * 当数据值大于该值时上传
     */
    @Builder.Default
    private double gt = 0;

    /**
     * 当数据值小于该值时上传
     */
    @Builder.Default
    private double lt = 0;

    /**
     * 当前后两个数据点值相差大于或者等于该值时
     */
    @Builder.Default
    private double st = 0;

    /**
     * 请求超时时间，默认25(单位：秒)，取值范围[5, 40]
     */
    @Builder.Default
    private int timeout = 25;


    @Override
    public String toUrl() {

        StringBuilder url = new StringBuilder(Config.getApiUrl());
        url.append("/nbiot").append("/observe");

        System.out.println(url);

        return url.toString();
    }

    @Override
    public String toJson() {

        Map<String, Object> maps = Maps.newHashMap();

        maps.put("imei", imei);
        maps.put("cancel", cancel);
        maps.put("obj_id", objId);

        if (objInstId >= 1) {
            maps.put("obj_inst_id", objInstId);
        }

        if (resId >= 1) {
            maps.put("res_id", resId);
        }
        if (pmin >= 1) {
            maps.put("pmin", pmin);
        }
        if (pmax >= 1) {
            maps.put("pmax", pmax);
        }
        if (gt >= 1) {
            maps.put("gt", gt);
        }

        if (lt >= 1) {
            maps.put("lt", lt);
        }
        if (st >= 1) {
            maps.put("st", st);
        }


        if (timeout >= 5 && timeout <= 40) {
            maps.put("timeout", timeout);
        }


        return JSON.toJSONString(maps);
    }


}
