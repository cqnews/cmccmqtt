package com.link510.nbiot.cmcc.domain.commands;

import com.link510.nbiot.cmcc.config.Config;
import com.link510.nbiot.cmcc.domain.BaseDTO;
import lombok.*;

/**
 * 发送命令
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SendCommandDTO implements BaseDTO {

    private static final long serialVersionUID = 8974039914108304592L;

    /**
     * 接收该数据的设备ID
     */
    @Builder.Default
    private String deviceId = "";

    /**
     * 是否需要设备应答，默认为0。
     * 0：最多发送一次，不关心设备是否响应
     * 1：至少发送一次，如果设备收到命令后没有应答，则会在下一次设备登录时若命令在有效期内（有效期定义参见timeout参数）则会重发该命令
     */
    @Builder.Default
    private Integer qos = 0;

    /**
     * 命令有效时间，默认0。
     * 0：在线命令，若设备在线,下发给设备，若设备离线，直接丢弃
     * >0： 离线命令，若设备在线，下发给设备，若设备离线，在当前时间加timeout时间内为有效期，有效期内，若设备上线，则下发给设备
     * 单位：秒
     * 有效范围：0~2678400
     */
    @Builder.Default
    private Integer timeout = 30;


    @Builder.Default
    private String cmd = "";


    /**
     * 转url
     *
     * @return 返回url
     */
    @Override
    public String toUrl() {

        StringBuilder url = new StringBuilder(Config.getApiUrl());

        url.append("/cmds");
        url.append("?device_id=").append(this.deviceId);
        url.append("&qos=").append(this.qos);
        if (this.qos >= 1 && this.timeout >= 1) {

            if (this.timeout >= 2678400) {
                this.timeout = 2678400;
            }
            url.append("&timeout=").append(this.timeout);
        }

        System.out.println(url);

        return url.toString();
    }

    @Override
    public String toJson() {
        return cmd;
    }




}
