package com.link510.nbiot.cmcc.domain;


import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OnenetMessageInfo implements Serializable {

    private static final long serialVersionUID = -8058105695461783624L;

    /**
     * 错误编码
     */
    private Integer errno = 0;

    /**
     * 错误信息
     */
    private String error = "";

    /**
     * 数据
     */
    private String data;

}
