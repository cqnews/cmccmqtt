package com.link510.nbiot.cmcc.domain;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * Vo
 *
 * @author cqnews
 */
public abstract class BaseOutDTO implements Serializable {

    private static final long serialVersionUID = 2153283186657778793L;

    private Logger logger = LoggerFactory.getLogger(BaseOutDTO.class);

    /**
     * 数据解析
     *
     * @param jsonStr 解析串
     * @return 返回
     */
    public BaseOutDTO parse(String jsonStr) {

        try {

            return JSON.parseObject(jsonStr, getClass());

        } catch (Exception ex) {

            logger.warn("数据解析失败,ex:" + ex.getMessage());

        }
        return null;
    }

}
