package com.link510.nbiot.cmcc.domain.device;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.link510.nbiot.cmcc.domain.BaseOutDTO;
import lombok.*;

/**
 * @author cqnews
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FindDeviceOutDTO extends BaseOutDTO {


    private static final long serialVersionUID = 7033178061494426272L;


    /**
     * 设备Id
     */
    @JSONField(name = "id")
    private String deviceId = "";

    /**
     * 表示LWM2M设备是否在线
     */
    private boolean online = false;

    /**
     * 设备接入协议，这里指定为: LWM2M，必填参数
     */
    private String protocol = "LWM2M";

    /**
     * 设备名称
     */
    private String title = "";

    /**
     * 设备描述
     */
    private String desc = "iniot device";

    /**
     * 创建时间
     */
    @JSONField(name = "create_time")
    private String createTime = "";


    /**
     * Auth_Code，由数字或字母组成，不超过16位
     */
    private boolean obsv = true;

    /**
     * 设备私密性（默认true)
     */
    @JSONField(name = "private")
    private boolean private2 = true;

    /**
     * 设备关联的图像或者二进制数据，详情见binary描述表
     */
    private JSONArray binary;

    /**
     * 设备标签，字符串数组,否
     */
    private JSONArray tags;


    // 设备地理位置，格式为：{"lon": 106, "lat": 29, "ele": 370}，可填参数
    private JSONObject location;

    /**
     * 设备鉴权信息，对应设备注册接口中的"sn"或者"mac"参数
     */
    @JSONField(name = "auth_info")
    private String authInfo = "";


    /**
     * 其他信息（JSON格式，可自定义）
     */
    private String other = "";


    /**
     * 设备数据流信息的json数组，见datastreams描述表
     */
    private JSONArray datastreams;


}
