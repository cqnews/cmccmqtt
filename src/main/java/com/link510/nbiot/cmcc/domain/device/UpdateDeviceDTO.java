package com.link510.nbiot.cmcc.domain.device;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.link510.nbiot.cmcc.config.Config;
import com.link510.nbiot.cmcc.domain.BaseDTO;
import lombok.*;

import java.util.Map;

/**
 * 更新设备
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateDeviceDTO implements BaseDTO {


    private static final long serialVersionUID = 427682995603584616L;

    /**
     * 设备Id
     */
    @Builder.Default
    private String deviceId = "";

    /**
     * 设备名称
     */
    @Builder.Default
    private String title = "";

    /**
     * 设备描述
     */
    @Builder.Default
    private String desc = "iniot device";

    /**
     * 设备标签，字符串数组,否
     */
    @Builder.Default
    private String tags = "";

    // 设备地理位置，格式为：{"lon": 106, "lat": 29, "ele": 370}，可填参数
    private JSONObject location;

    /**
     * 设备私密性（默认true)
     */
    @JSONField(name = "private")
    @Builder.Default
    private boolean private2 = true;


    @JSONField(name = "auth_info")
    @Builder.Default
    private String authInfo = "";

    /**
     * 其他信息（JSON格式，可自定义）
     */
    @Builder.Default
    private String other = "";


    @Override
    public String toUrl() {
        StringBuilder url = new StringBuilder(Config.getApiUrl());
        url.append("/devices").append(this.deviceId);
        System.out.println(url);
        return url.toString();
    }

    @Override
    public String toJson() {

        Map<String, Object> maps = Maps.newHashMap();

        maps.put("title", title);

        if (!Strings.isNullOrEmpty(desc)) {
            maps.put("desc", desc);
        }

        if (!Strings.isNullOrEmpty(tags)) {
            maps.put("tags", tags);
        }

        if (location != null) {
            maps.put("location", this.location.toJSONString());
        }

        if (!private2) {
            maps.put("private", private2);
        }


        if (Strings.isNullOrEmpty(authInfo)) {
            maps.put("auth_info", authInfo);
        }

        if (!Strings.isNullOrEmpty(other)) {
            maps.put("other", other);
        }


        return JSON.toJSONString(maps);
    }
}
