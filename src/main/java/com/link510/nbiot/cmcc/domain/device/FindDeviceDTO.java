package com.link510.nbiot.cmcc.domain.device;

import com.link510.nbiot.cmcc.config.Config;
import com.link510.nbiot.cmcc.domain.BaseDTO;
import lombok.*;

/**
 * 查找设备
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FindDeviceDTO implements BaseDTO {

    private static final long serialVersionUID = -3013499016680477348L;

    /**
     * 设备编号
     */
    @Builder.Default
    private String deviceId = "";


    @Override
    public String toUrl() {
        StringBuilder url = new StringBuilder(Config.getApiUrl());
        url.append("/devices/").append(deviceId);

        System.out.println(url);

        return url.toString();
    }
}
