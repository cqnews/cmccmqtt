package com.link510.nbiot.cmcc.domain.commands;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.collect.Maps;
import com.link510.nbiot.cmcc.config.Config;
import com.link510.nbiot.cmcc.domain.BaseDTO;
import lombok.*;

import java.util.Map;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExecuteCommandDTO implements BaseDTO {

    private static final long serialVersionUID = 1042185541455553111L;


    /**
     * 设备imei号，平台唯一，必填参数
     */
    @Builder.Default
    private String imei = "";

    /**
     * 设备的object id，根据终端设备sdk确定
     */
    @JSONField(name = "obj_id")
    @Builder.Default
    private Integer objId = 3200;

    /**
     * nbiot设备object下具体的一个实例id,根据终端设备sdk确定
     */
    @JSONField(name = "obj_inst_id")
    @Builder.Default
    private Integer objInstId = 0;


    /**
     * nbiot设备的资源id,根据终端设备sdk确定
     */
    @JSONField(name = "res_id")
    @Builder.Default
    private Integer resId = 0;

    /**
     * 命令字符串，大小限制2k
     */
    @Builder.Default
    private String args = "";

    /**
     * 请求超时时间，默认25(单位：秒)，取值范围[5, 40]
     */
    @Builder.Default
    private Integer timeout = 25;


    @Override
    public String toUrl() {

        StringBuilder url = new StringBuilder(Config.getApiUrl());

        url.append("/nbiot").append("/execute");
        url.append("?imei=").append(this.getImei());
        url.append("&obj_id=").append(this.getObjId());
        url.append("&obj_inst_id=").append(this.getObjInstId());
        url.append("&res_id=").append(this.getResId());

        if (timeout > 25 && timeout <= 40) {
            url.append("&timeout=").append(this.timeout);
        }

        System.out.println(this.toString());

        System.out.println(url);

        return url.toString();
    }

    @Override
    public String toJson() {
        Map<String, Object> maps = Maps.newHashMap();
        maps.put("args", args);
        return JSON.toJSONString(maps);
    }
}
