package com.link510.nbiot.cmcc.domain.device;

import com.link510.nbiot.cmcc.config.Config;
import com.link510.nbiot.cmcc.domain.BaseDTO;
import lombok.*;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DeleteDeviceDTO implements BaseDTO {

    private static final long serialVersionUID = -5429728064604150690L;

    /**
     * 设备Id
     */
    @Builder.Default
    private String deviceId = "";


    @Override
    public String toUrl() {

        StringBuilder url = new StringBuilder(Config.getApiUrl());
        url.append("/devices/").append(deviceId);

        System.out.println(url);

        return url.toString();
    }
}
