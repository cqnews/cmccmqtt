package com.link510.nbiot.cmcc.domain.commands;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.link510.nbiot.cmcc.config.Config;
import com.link510.nbiot.cmcc.domain.BaseDTO;
import com.link510.nbiot.cmcc.domain.BaseOutDTO;
import lombok.*;

import java.util.Map;

/**
 * 发送命令
 *
 * @author cqnews
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor

public class CacheCommandOutDTO extends BaseOutDTO {

    private static final long serialVersionUID = 8974039914108304592L;

    /**
     * 离线命令uuid
     */
    private String uuid = "";


}
