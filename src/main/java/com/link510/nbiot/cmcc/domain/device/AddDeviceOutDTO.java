package com.link510.nbiot.cmcc.domain.device;

import com.alibaba.fastjson.annotation.JSONField;
import com.link510.nbiot.cmcc.domain.BaseOutDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author cqnews
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddDeviceOutDTO extends BaseOutDTO {

    private static final long serialVersionUID = -3656495531766897980L;

    @JSONField(name = "device_id")
    private String deviceId = "";


}
