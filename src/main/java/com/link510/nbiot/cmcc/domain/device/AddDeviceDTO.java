package com.link510.nbiot.cmcc.domain.device;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.link510.nbiot.cmcc.config.Config;
import com.link510.nbiot.cmcc.domain.BaseDTO;
import lombok.*;

import java.util.Map;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddDeviceDTO implements BaseDTO {

    private static final long serialVersionUID = 6617371527156837565L;

    /**
     * 设备名称
     */
    @Builder.Default
    private String title = "";

    /**
     * 设备描述
     */
    @Builder.Default
    private String desc = "iniot device";

    /**
     * 设备标签，字符串数组,否
     */
    @Builder.Default
    private String tags = "";


    // 设备地理位置，格式为：{"lon": 106, "lat": 29, "ele": 370}，可填参数
    private JSONObject location;

    /**
     * 设备私密性（默认true)
     */
    @JSONField(name = "private")
    @Builder.Default
    private boolean private2 = true;

    /**
     * Auth_Code，由数字或字母组成，不超过16位
     */
    @Builder.Default
    @JSONField(name = "auth_info")
    private String authInfo = "";


    /**
     * 其他信息（JSON格式，可自定义）
     */
    @Builder.Default
    private String other = "";


    public AddDeviceDTO(String title) {
        this.title = title;
    }

    @Override
    public String toUrl() {
        StringBuilder url = new StringBuilder(Config.getApiUrl());
        url.append("/devices");
        System.out.println(url);
        return url.toString();
    }

    @Override
    public String toJson() {

        Map<String, Object> maps = Maps.newHashMap();

        maps.put("title", title);

        if (!Strings.isNullOrEmpty(desc)) {
            maps.put("desc", desc);
        }

        if (!Strings.isNullOrEmpty(tags)) {
            maps.put("tags", tags);
        }

        if (location != null) {
            maps.put("location", this.location.toJSONString());
        }

        if (!private2) {
            maps.put("private", private2);
        }

        maps.put("auth_info", authInfo);

        if (!Strings.isNullOrEmpty(authInfo)) {
            maps.put("auth_info", authInfo);
        }

        if (!Strings.isNullOrEmpty(other)) {
            maps.put("other", other);
        }

        return JSON.toJSONString(maps);
    }
}
