package com.link510.nbiot.cmcc.domain;


import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.collect.Maps;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

/**
 * @author cqnews
 */

public interface BaseDTO extends Serializable {



    /**
     * 转url
     *
     * @return 返回url
     */
    String toUrl();

    /**
     * 转maps
     *
     * @return 返回maps
     */
    default String toJson() {
        return null;
    }

}
