package com.link510.nbiot.cmcc.exption;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by zhuocongbin
 * date 2018/3/15
 */
@AllArgsConstructor
public enum NBStatus {
    /**
     * nbiot
     */
    HTTP_REQUEST_ERROR(1, "http request error"),
    LOAD_CONFIG_ERROR(2, "load config error"),
    PARSER_DATA_ERROR(3, "load config error"),
    NBIOT_CALL_ERROR(4, "load config error");

    @Getter
    private Integer code;

    @Getter
    private String error;




}
