package com.link510.nbiot.cmcc.exption;

/**
 * nbiot异常
 *
 * @author cqnews
 */
public class OnenetNBException extends RuntimeException {

    private static final long serialVersionUID = -8790143100635451745L;
    private NBStatus status;

    private String message = null;

    public OnenetNBException(NBStatus status) {
        this.status = status;
    }

    public OnenetNBException(NBStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getCode() {
        return status.getCode();
    }

    public String getError() {
        if (message != null) {
            return status.getError() + ": " + message;
        } else {
            return status.getError();
        }
    }
}
