package com.link510.nbiot.cmcc;

import com.link510.nbiot.cmcc.exption.OnenetNBException;

import java.util.Map;

public interface NBIoTClient {


    /**
     * Post提交数据
     *
     * @param <T>  类型
     * @param url  url地址
     * @param body 参数
     * @param clzz 类型
     * @return 返回
     * @throws OnenetNBException OnenetNBException
     */
    <T> T restJsonPost(String url, String body, Class<T> clzz) throws OnenetNBException;


    /**
     * Post提交数据
     *
     * @param <T>  类型
     * @param url  url地址
     * @param body 参数
     * @param clzz 类型
     * @return 返回
     * @throws OnenetNBException OnenetNBException
     */
    <T> T restBinaryPost(String url, String body, Class<T> clzz) throws OnenetNBException;

    /**
     * GET提交数据
     *
     * @param <T>  类型
     * @param url  url地址
     * @param clzz 类型
     * @return 返回
     * @throws OnenetNBException OnenetNBException
     */
    <T> T restGet(String url, Class<T> clzz) throws OnenetNBException;


    /**
     * DELETE提交数据
     * @param <T>  类型
     * @param url  url地址
     * @param clzz 类型
     * @return 返回
     */
    <T> T restDelete(String url, Class<T> clzz) throws OnenetNBException;


    /**
     * 获取ApiKey
     */
    String getApiKey();


    /**
     * 获取ApiSecret
     */
    String getApiSecret();

}
