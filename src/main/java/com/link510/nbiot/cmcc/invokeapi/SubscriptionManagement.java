package com.link510.nbiot.cmcc.invokeapi;


import com.link510.nbiot.cmcc.NBIoTClient;
import com.link510.nbiot.cmcc.domain.OnenetMessageInfo;
import com.link510.nbiot.cmcc.domain.subscription.SendSubscriptionDTO;
import com.link510.nbiot.cmcc.exption.NBStatus;
import com.link510.nbiot.cmcc.exption.OnenetNBException;

/**
 * 消息订阅管理
 */
public class SubscriptionManagement extends BaseManagement{

    private NBIoTClient nbIoTClient;

    private SubscriptionManagement() {

    }

    public SubscriptionManagement(NBIoTClient nbIoTClient) {
        this.nbIoTClient = nbIoTClient;
    }

    public boolean send(SendSubscriptionDTO subscriptionDTO) throws OnenetNBException {
        try {

            OnenetMessageInfo messageInfo = this.nbIoTClient.restJsonPost(subscriptionDTO.toUrl(), subscriptionDTO.toJson(), OnenetMessageInfo.class);

            validateMessageInfo(messageInfo);

            return true;
        } catch (OnenetNBException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new OnenetNBException(NBStatus.PARSER_DATA_ERROR, "数据解析异常");
        }
    }



}
