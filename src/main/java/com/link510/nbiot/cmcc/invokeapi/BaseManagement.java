package com.link510.nbiot.cmcc.invokeapi;

import com.link510.nbiot.cmcc.domain.OnenetMessageInfo;
import com.link510.nbiot.cmcc.domain.commands.CacheCommandOutDTO;
import com.link510.nbiot.cmcc.domain.commands.SendCommandDTO;
import com.link510.nbiot.cmcc.exption.NBStatus;
import com.link510.nbiot.cmcc.exption.OnenetNBException;

public abstract class BaseManagement {


    protected static void validateMessageInfo(OnenetMessageInfo messageInfo) throws OnenetNBException {

        if (messageInfo == null) {
            throw new OnenetNBException(NBStatus.PARSER_DATA_ERROR, "数据解析异常");
        }

        if (!messageInfo.getErrno().equals(0)) {
            throw new OnenetNBException(NBStatus.NBIOT_CALL_ERROR, messageInfo.getError());
        }
    }

}
