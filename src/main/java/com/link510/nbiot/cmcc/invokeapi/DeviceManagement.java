package com.link510.nbiot.cmcc.invokeapi;

import com.alibaba.fastjson.JSON;
import com.link510.nbiot.cmcc.NBIoTClient;
import com.link510.nbiot.cmcc.domain.device.*;
import com.link510.nbiot.cmcc.domain.OnenetMessageInfo;
import com.link510.nbiot.cmcc.exption.NBStatus;
import com.link510.nbiot.cmcc.exption.OnenetNBException;

/**
 * 设备管理
 */
public class DeviceManagement extends BaseManagement {

    private NBIoTClient nbIoTClient;

    private DeviceManagement() {

    }

    public DeviceManagement(NBIoTClient nbIoTClient) {
        this.nbIoTClient = nbIoTClient;
    }


    /**
     * 新增设备
     *
     * @param deviceDTO 设备新增设备
     * @return 返回注册消息
     * @throws OnenetNBException OnenetNBException
     */
    public AddDeviceOutDTO add(AddDeviceDTO deviceDTO) throws OnenetNBException {

        try {

            OnenetMessageInfo messageInfo = this.nbIoTClient.restJsonPost(deviceDTO.toUrl(), deviceDTO.toJson(), OnenetMessageInfo.class);

            validateMessageInfo(messageInfo);

            AddDeviceOutDTO addDeviceOutDTO = JSON.parseObject(messageInfo.getData(), AddDeviceOutDTO.class);

            if (addDeviceOutDTO == null) {
                throw new OnenetNBException(NBStatus.PARSER_DATA_ERROR, "数据解析异常2");
            }

            return addDeviceOutDTO;
        } catch (OnenetNBException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new OnenetNBException(NBStatus.PARSER_DATA_ERROR, "数据解析异常");
        }
    }

    /**
     * 更新设备信息
     *
     * @param deviceDTO 设备信息
     * @return 返回更新内容
     * @throws OnenetNBException OnenetNBException
     */
    public boolean update(UpdateDeviceDTO deviceDTO) throws OnenetNBException {

        try {

            OnenetMessageInfo messageInfo = this.nbIoTClient.restJsonPost(deviceDTO.toUrl(), deviceDTO.toJson(), OnenetMessageInfo.class);

            validateMessageInfo(messageInfo);

            return true;
        } catch (OnenetNBException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new OnenetNBException(NBStatus.PARSER_DATA_ERROR, "数据解析异常");
        }
    }

    /**
     * 查找设备
     *
     * @param deviceId 设备Id
     * @return 返回消息
     * @throws OnenetNBException 异常
     */
    public FindDeviceOutDTO find(String deviceId) throws OnenetNBException {


        try {

            OnenetMessageInfo messageInfo = this.nbIoTClient.restGet(new FindDeviceDTO(deviceId).toUrl(), OnenetMessageInfo.class);

            validateMessageInfo(messageInfo);

            FindDeviceOutDTO findDeviceOutDTO = JSON.parseObject(messageInfo.getData(), FindDeviceOutDTO.class);

            if (findDeviceOutDTO == null) {
                throw new OnenetNBException(NBStatus.PARSER_DATA_ERROR, "数据解析异常2");
            }
            return findDeviceOutDTO;

        } catch (OnenetNBException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new OnenetNBException(NBStatus.PARSER_DATA_ERROR, "数据解析异常");
        }
    }



    /**
     * 删除设备
     *
     * @param deviceId 设备Id
     * @return 返回消息
     * @throws OnenetNBException 异常
     */
    public boolean delete(String deviceId) throws OnenetNBException {

        try {

            OnenetMessageInfo messageInfo = this.nbIoTClient.restDelete(new DeleteDeviceDTO(deviceId).toUrl(), OnenetMessageInfo.class);

            validateMessageInfo(messageInfo);

            return true;


        } catch (OnenetNBException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new OnenetNBException(NBStatus.PARSER_DATA_ERROR, "数据解析异常");
        }
    }
}
