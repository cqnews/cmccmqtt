package com.link510.nbiot.cmcc.config;

import com.link510.nbiot.cmcc.exption.NBStatus;
import com.link510.nbiot.cmcc.exption.OnenetNBException;

import java.io.IOException;
import java.util.Properties;

public class Config {

    public static final String url;


    static {
        Properties properties = new Properties();
        try {
            properties.load(Config.class.getClassLoader().getResourceAsStream("config.properties"));
            url = (String) properties.get("apiUrl");
        } catch (IOException e) {
            throw new OnenetNBException(NBStatus.LOAD_CONFIG_ERROR);
        }
    }

    public static String getApiUrl() {
        return url;
    }

}
